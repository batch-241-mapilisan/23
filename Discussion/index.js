// Creating objects using constructor functions
/*
- Creates a reusable function to create several objects that have the same data structure
- Useful for creating multiple instances / copies of an object
- An instance is a concrete occurence of any object which emphasizes on the distinct / unique identity of it
Syntax:
    function ObjectName(keyA, keyB) {
        this.keyA = keyA,
        this.keyB = keyB
    }
*/

function Laptop(name, manufactureDate) {
    this.name = name;
    this.manufactureDate = manufactureDate;
}

function Student(firstName, lastName, yearOld, city) {
    this.name = `${firstName} ${lastName}`;
    this.age = yearOld;
    this.residence = city;
}
/*
- The "this" keyword allows us to assign new object properties by associating them with values received from a constructor function
*/

// This is an instance of the Laptop Object
let laptop = new Laptop("Dell", 2022)
console.log(laptop);

let elaine = new Student("Elaine", "SJ", 12, "Mars");
console.log(elaine);